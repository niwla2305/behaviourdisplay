apt-get update
apt-get upgrade -y

cd /opt/bd
git pull
git submodule update --remote --merge

pip3 install -r backend/requirements.txt

cd /opt/bd/frontend
wget -O frontend.zip https://gitlab.com/Niwla23/bd_frontend/-/jobs/artifacts/master/download?job=build
rm -rf dist
unzip frontend.zip
rm frontend.zip

cd /opt/bd

cp /opt/bd/extra/nginx-site /etc/nginx/sites-available/default
cp extra/bd_backend.service /etc/systemd/system/
systemctl daemon-reload
systemctl restart mongod
systemctl restart nginx
systemctl restart bd_backend
nginx -s reload