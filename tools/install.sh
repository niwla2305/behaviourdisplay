echo Welcome to the behaviourdisplay installer!
read -p "What should be the teacher password? " TEACHER_PASSWORD

curl -s https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list
apt-get update
apt-get upgrade -y
apt-get install -y mongodb-org nginx git npm python3-pip python3 wget


git clone https://gitlab.com/niwla23/behaviourdisplay.git /opt/bd --recurse-submodules -j8

cd /opt/bd

cp extra/bd_backend.service /etc/systemd/system/


# Configure backend
cd /opt/bd/backend
pip3 install -r requirements.txt
pip3 install gunicorn

echo "BD_PASSWORD="$TEACHER_PASSWORD > .env

# Configure frontend
cd /opt/bd/frontend
wget -O frontend.zip https://gitlab.com/Niwla23/bd_frontend/-/jobs/artifacts/master/download?job=build
unzip frontend.zip
rm frontend.zip

cp /opt/bd/extra/bd_viewer.desktop /etc/xdg/autostart/bd_admin.desktop

# Configure NGINX
cp /opt/bd/extra/nginx-site /etc/nginx/sites-available/default
nginx -s reload

systemctl daemon-reload
systemctl enable mongod
systemctl enable bd_backend
systemctl start mongod
systemctl start bd_backend
